import java.util.Scanner;
public class Bankers{
    private int need_Matrix[][],allocate_Matrix[][],max_Matrix[][],avail_Matrix[][],num_process,num_res;

    private void input(){
        Scanner sc=new Scanner(System.in);
        System.out.print("Enter your number processes and resources : ");
        num_process=sc.nextInt();  //get number of process
        num_res=sc.nextInt();  //get number of resources
        need_Matrix=new int[num_process][num_res];  //initializing arrays
        max_Matrix =new int[num_process][num_res];
        allocate_Matrix=new int[num_process][num_res];
        avail_Matrix=new int[1][num_res];

        System.out.println("Enter the allocation matrix:");
        for(int i=0;i<num_process;i++)
            for(int j=0;j<num_res;j++)
                allocate_Matrix[i][j]=sc.nextInt();  //get the allocation matrix

        System.out.println("Enter maximum matrix:");
        for(int i=0;i<num_process;i++)
            for(int j=0;j<num_res;j++)
                max_Matrix[i][j]=sc.nextInt();  //max matrix

        System.out.println("Enter available matrix:");
        for(int j=0;j<num_res;j++)
            avail_Matrix[0][j]=sc.nextInt();  //get the available matrix

        sc.close();
    }

    private int[][] calc_need_Matrix(){
        for(int i=0;i<num_process;i++)
            for(int j=0;j<num_res;j++)  //we calculate the need matrix
                need_Matrix[i][j]=max_Matrix[i][j]-allocate_Matrix[i][j];

        return need_Matrix;
    }

    private boolean check(int i){
        //check if all its possible to locate all resources to ith process
        for(int j=0;j<num_res;j++)
            if(avail_Matrix[0][j]<need_Matrix[i][j])
                return false;

        return true;
    }

    public void safeOrNot(){
        input();
        calc_need_Matrix();
        boolean done[]=new boolean[num_process];
        int j=0;

        while(j<num_process){  //while we can allocate to all processes
            boolean allocated=false;
            for(int i=0;i<num_process;i++)
                if(!done[i] && check(i)){  //checking to allocate
                    for(int k=0;k<num_res;k++)
                        avail_Matrix[0][k]=avail_Matrix[0][k]-need_Matrix[i][k]+max_Matrix[i][k];
                    System.out.println("Allocated process is : "+i);
                    allocated=done[i]=true;
                    j++;
                }
            if(!allocated) break;  //if no allocation happens, break
        }
        if(j==num_process)  //if we can allocate resources to all processes
            System.out.println("\nThe processes are safely allocated");
        else
            System.out.println("All processes cant be allocated safely");
    }

    public static void main(String[] args) {
        new Bankers().safeOrNot();
    }
}